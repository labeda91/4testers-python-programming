# Zadanie 1: Utwórz funkcję, która przyjmie słownik opisujący gracza i drukuje jego opis. #
def gamer_info_printer(player_data):
    print(f"The player {player_data['nick']} is of type {player_data['type']} and has {player_data['exp_points']} EXP")


# Zadanie 2: Napisz funkcję, która wyszukuje liczby podzielne przez inną liczbę w zakresie od-do liczb całkowitych: #
def find_divisible_subset(number_from, number_to, divisible_by):
    list_of_number = []
    for n in range(number_from, number_to + 1):
        if n % divisible_by == 0:
            list_of_number.append(n)
    return list_of_number


# Zadanie 3: Stwórz funkcję przyjmującą listę temperatur (w st. Celsiusza) i zwracającą listę w Fahrenheitach #
def temperature_in_celsius_to_fahrenheit(temp_celsius):
    return 9 / 5 * temp_celsius + 32


def temperature_in_celsius_ans_fahrenheit(temperature_in_celsius_list):
    temperatur_in_fahrenheit_list = []
    for temp_celsius in temperature_in_celsius_list:
        converted_temp = round(temperature_in_celsius_to_fahrenheit(temp_celsius), 2)  # round - zaokrąglenie do 2 msc.
        temperatur_in_fahrenheit_list.append(converted_temp)
    return temperatur_in_fahrenheit_list


if __name__ == '__main__':
    player_details = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 5000
    }

    player_2_details = {
        "nick": "mr_buggy",
        "type": "lead",
        "exp_points": 8000
    }

gamer_info_printer(player_details)
gamer_info_printer(player_2_details)

print(find_divisible_subset(1, 100, 9))

spring_temperatures = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
print(temperature_in_celsius_ans_fahrenheit(spring_temperatures))
