def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


# kwadrat pierszych 10 liczb całkowitych - z listy poniżej #
def print_first_ten_intigers_squared_v1(lists):
    for intiger in lists:
        print(intiger ** 2)


# kwadrat liczb z dowolnego wyznaczonego zakresu #
def print_first_ten_intigers_squared_v2():
    for intiger in range(1, 21):  # od 1 do 10 włącznie #
        print(intiger ** 2)


# Drukuje liczby od 0 (i) do limiting number #
def print_number_from_0_to_limit(limiting_number):
    for i in range(limiting_number + 1):
        print(i)


# Wydrukuj liczby od 1 do 30 podzielne przez 7 #
def print_numbers_divisible_by_7(from_number, to_number):
    for i in range(from_number, to_number + 1):  # iteracja po i przebiega zawsze do wartości o 1 mniejszej niż podana #
        if i % 7 == 0:
            print(i)


# funkcję przyjmującą listę temperatur (w st. Celsiusza) i drukuj każdą z temperatur w stopniach Celsiusza i Fahrenheitach. #
def temperature_in_celsius_to_fahrenheit(temp_celsius):
    return 9 / 5 * temp_celsius + 32


def print_each_temperature_in_celsius_ans_fahrenheit(temperature_in_celsius_list):
    for temp_celsius in temperature_in_celsius_list:
        converted_temp = round(temperature_in_celsius_to_fahrenheit(temp_celsius), 2)  # round - zaokrąglenie do 2 msc.
        print(f"Temperature in Celsius: {temp_celsius}; temperature in Fahrenheit: {converted_temp}")


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "tosia", "niCki", "STEPHANE"]
    print_each_student_name_capitalized(list_of_students)

    print("-----")

    first_ten_intigers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print_first_ten_intigers_squared_v1(first_ten_intigers)

    print("-----")

    print_first_ten_intigers_squared_v2()

    print("-----")

    print_number_from_0_to_limit(10)

    print("-----")

    print_numbers_divisible_by_7(0, 50)

    print("-----")

    spring_temperatures = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_each_temperature_in_celsius_ans_fahrenheit(spring_temperatures)
