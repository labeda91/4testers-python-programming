import random
from datetime import datetime

female_firstname = ["Kate", "Agnieszka", "Anna", "Maria", "Joss", "Eryka"]
male_firstname = ["James", "Bob", "Jan", "Hans", "Orestes", "Saturnin"]
lastname = ["Smith", "Kowalski", "Yu", "Bona", "Muster", "Skinner", "Cox", "Brick", "Malina"]
countries = ["Poland", "United Kingdom", "Germany", "France", "Other"]

all_list_size = 10
list_size_female = 5
list_size_male = 5


def generate_random_female_dictionary():
    return {
        "firstname": random.choice(female_firstname),
        "lastname": random.choice(lastname),
        "country": random.choice(countries),
        "age": random.randint(5, 45),
    }


def generate_random_male_dictionary():
    return {
        "firstname": random.choice(male_firstname),
        "lastname": random.choice(lastname),
        "country": random.choice(countries),
        "age": random.randint(5, 45),
    }


def generate_list_of_people_dictionaries():
    output_list = []
    for dictionary in range(list_size_female):
        output_list.append(generate_random_female_dictionary())
    for dictionary in range(list_size_male):
        output_list.append(generate_random_male_dictionary())

    for m in range(all_list_size):
        output_list[m]["email"] = f"{output_list[m]['firstname']}.{output_list[m]['lastname']}@example.com".lower()

    for a in range(all_list_size):
        if output_list[a]["age"] >= 18:
            output_list[a]["adult"] = True
        else:
            output_list[a]["adult"] = False

    for y in range(all_list_size):
        output_list[y]["birth_year"] = datetime.now().year - output_list[y]["age"]

    return output_list


def person_presentation(list_of_people_dictionary):
    for person_dictionary in list_of_people_dictionary:
        print(f"Hi! I'm {person_dictionary['firstname']} {person_dictionary['lastname']}. I come from {person_dictionary['country']} and I Was "
              f"born in {person_dictionary['birth_year']}.")


if __name__ == '__main__':
    list_of_people = generate_list_of_people_dictionaries()

    print(list_of_people)

    person_presentation(list_of_people)
