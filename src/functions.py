import random
import string


# Powitanie #
def say_hello(name):
    print("Cześć", name)
    print(f"Co u Ciebie {name}?")


say_hello("Agnieszka")
say_hello("Zbyszek")


# Zamiana na wielkie litery #
def upper_word(word):
    return word.upper()


word_do_przerobienia = "wielkie litery"

print(f"Słowo '{word_do_przerobienia}' zapisane wielkimi literami to:",
      upper_word(word_do_przerobienia))


# Sumowanie licz #
def add(a, b):
    return a + b


number1 = 5
number2 = 6

print(f"Suma liczb {number1} i {number2} to:", add(number1, number2))


# pole koła #
def circle_area(r):
    return 3.1415 * r ** 2


promien = number1 + number2

print(f"Pole kola o promieniu {promien} to:", circle_area(promien))


# kwadrat dowolnej liczby v1 #
def kwadrat_liczby(liczba):
    return liczba ** 2


liczba1 = 2
liczba2 = 16
liczba3 = 2.77

kwadrat1 = kwadrat_liczby(liczba1)
kwadrat2 = kwadrat_liczby(liczba2)
kwadrat3 = kwadrat_liczby(liczba3)

print("Kwadrat dowolnej liczby - metoda 1")
print(f"Kwadrat z {liczba1} to:", kwadrat1)
print(f"Kwadrat z {liczba2} to:", kwadrat2)
print(f"Kwadrat z {liczba3} to:", kwadrat3)


# kwadrat dowolnej liczby v2 #
def kwadrat(liczba):
    print(liczba ** 2)


print("Kwadrat dowolnej liczby - metoda 2")
kwadrat(0), kwadrat(16), kwadrat(2.55)


# objętość prostopadłościanu o bokach a, b, c #
def objetosc_prostopadloscianu(a, b, c):
    return a * b * c


print(f"Objetosc prostopadloscianu o wymiarach 3, 5, 7 to:",
      objetosc_prostopadloscianu(3, 5, 7))


# zamiana stopni Celsjusza na Fahrenheita #
def stopnie_fahr(stopnie):
    return stopnie * 9 / 5 + 32


stopnie_cel = 15.8

print(f"{stopnie_cel} stopni Celsjusza to:",
      stopnie_fahr(stopnie_cel), "stopni Fahrenheita.")


# Napis powitalny #
def napis_powitalny(imie, miasto):
    print(f"Witaj {imie.upper()}! Miło Cię widzieć w naszym mieście: {miasto.upper()}!")


napis_powitalny("Michał", "Toruń")
napis_powitalny("Beata", "Gdynia")


# generowanie losowego hasła #
def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def generated_data_email(email):
    return {
        "email": email,
        "password": get_random_string(25)
    }

print(generated_data_email("labeda91@gmail.com"))
