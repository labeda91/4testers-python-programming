first_name = "Paweł"
last_name = "Łabęda"
email = "labeda91@gmail.com"

my_bio_using_f_string = (f"Mam na imię {first_name}.\nMoje "
                         f"nazwisko to {last_name}. Mój email "
                         f"to {email}.")
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to: {first_name}")

circle_radious = 16

area_of_a_circle_with_radious = 3.14 * circle_radious ** 2

circumference_of_a_circle_with_radious = 2 * 3.14 * circle_radious

print("Promień jest:", circle_radious)

print(f"Area of a circle with radious {circle_radious}:",
      area_of_a_circle_with_radious)

print(f"Circumference of a circle with radious {circle_radious}:",
      circumference_of_a_circle_with_radious)

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13

print(long_mathematical_expression)
