# funkcja mapująca listę i odwracająca słowa #
def get_words_reversed(list_of_words):
    output_list = []
    for word in list_of_words:
        output_list.append(word[::-1])
    return output_list


# wykorzystanie pythonowych list comprehension #
def get_words_reversed_v2(list_of_words):
    return [word[::-1] for word in list_of_words]

# zwróc listę słów zawierających literę a lub A #
def get_words_containing_letter_a(list_of_words):
    return [word for word in list_of_words if 'a' in word.lower()] # zmieniamy swłowa na małe litery aby znaleźć małe i duże a #


if __name__ == '__main__':
    list_words = ["Admin", "labeda", "mysz", "jablko", "lot"]

    print(get_words_reversed(list_words))
    print(get_words_reversed_v2(list_words))
    print(get_words_containing_letter_a(list_words))
