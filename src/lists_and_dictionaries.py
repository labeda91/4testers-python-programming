if __name__ == '__main__':
    # LISTY - przechowują dane jednego typu #
    shopping_list = ['oranges', 'water', 'chicken', 'potatoes']
    print(shopping_list[0])  # 0 to pierszy element listy #
    print(shopping_list[-1])  # -1 to ostatni element listy #

    # dodanie elementu na koniec listy #
    shopping_list.append('lemons')
    shopping_list.append('tea')
    print(f"Shopping list now is: {shopping_list}")
    print(f"Last element of list is: {shopping_list[-1].upper()}")  # -1 to ostatni element listy #

    # policzenie elementów listy #
    shopping_list_lenght = len(shopping_list)
    print(f"The new number of shopping list is: {shopping_list_lenght}")

    # wyciąganie pierwszego i ostatniego elementu listy #
    first_product = shopping_list[0]
    last_product = shopping_list[shopping_list_lenght - 1]  # lub może być [-1] #
    print(f"First product is: {first_product.upper()} and last product is: {last_product.upper()}")

    # wyciągnięcie tylko 3 pierwszych elementów #
    firs_three_shopping_items = shopping_list[0:3]
    print(f"First thee shopping product is: {firs_three_shopping_items}")

    # policzenie elementów krótszej listy #
    short_shopping_list_lenght = len(firs_three_shopping_items)
    print(f"The number of short shopping list is: {short_shopping_list_lenght}")

    # zamiana elementu listy #
    shopping_list[1] = "beer"
    print(f"Shopping list now is: {shopping_list}")

    # dodanie nowego elementu do listy / pozostałe przesuwają sie w lewo #
    shopping_list.insert(1, "ketchup")
    print(f"Shopping list now is: {shopping_list}")

    # SŁOWNIK - przechowują dane różnego typu #
    animal = {
        "name": "Burek",
        "kind": "dog",
        "age": 7,
        "male": True
    }

    # wydrukowanie danych ze słownika #
    print("Dog age:", animal["age"])

    # przypisanie nowej wartości do słownika #
    animal["age"] = 10
    print("Dog age new:", animal["age"])

    # dodanie nowej wartości do słownika #
    animal["owner"] = "Staszek"
    print(animal)


    # Zmienna zawierającą imię, wiek oraz listę dwóch hobby w postaci słownika #
    friend = {
        "name": "Natalia",
        "age": 29,
        "hobby": ["shopping", "drive bicycle"]
    }

    # dodatnie do liczby znajdującej się w słowniku #
    friend["age"] += 3
    print(friend)

    # dodanie do listy znajdującej się w słowniku #
    friend["hobby"].append("archery")
    print(friend)

    # LISTY SŁOWNIKÓW #
    animals = [
        {
            "kind": "dog",
            "age": 2,
            "male": True
        },
        {
            "kind": "cat",
            "age": 3,
            "male": False
        },
        {
            "kind": "fish",
            "age": 1,
            "male": True
        }
    ]
    print(animals[0])  # pierwszy słownik #

    # wyciąganie konkretnej wartości ze słownika - wiek ryby #
    animal_fish = animals[2]
    animal_fish_age = animal_fish["age"]
    print(f"The age of the fish is: {animal_fish_age}")

    # wyciąganie konkretnej wartości ze słownika - krótka wersja - wiek ryby #
    print(f"The age of the fish is: {animals[2]['age']}")

    # dodanie kolejnego elementu do listy #
    animals.append(
        {
            "kind": "zebra",
            "age": 7,
            "male": False
        }
    )
    print(animals)
