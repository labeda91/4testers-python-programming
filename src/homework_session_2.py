# zadanie z generowania adresów email #
def generated_adress_email(first_name, last_name):
    print(f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


# funkcja która przyjmuje trzy liczby i zwróci listę zawierającą te liczby pomnożone przez 3 #
def multiplying_by_three(number_1, number_2, number_3):
    return [number_1 * 3, number_2 * 3, number_3 * 3]


# funkcja która przyjmuje listę ocen ucznia i zwraca jej średnią zaokrąglona do 2 miejsc po
# przecinku #
def average_grade(grade_list):
    return round(sum(grade_list) / len(grade_list), 2)


if __name__ == '__main__':
    emails = ["a@example.com", "b@example.com"]

    # długość listy #
    print(f"The number of emails list is: {len(emails)}")

    # pierwszy i ostatni element listy #
    print(f"First email is: {emails[0]} and last email is: {emails[-1]}")

    # dodanie nowego adresu na końcu #
    emails.append("cde@example.com")
    print(f"Email list now is: {emails}")


    generated_adress_email("Janusz", "Nowak")
    generated_adress_email("Barbara", "Kowalska")


    print(multiplying_by_three(4, 5, 8))

    # funkcja która przyjmuje listę ocen ucznia i zwraca jej średnią #
    print(f"Avarage grade is:", average_grade([6, 2, 4, 3, 2, 5]))
