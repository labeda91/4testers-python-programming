def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


# wersja długa #
def words_with_letter_a_long(list_of_words):
    output_list = []
    for word in list_of_words:
        if 'a' in word.lower():
            output_list.append(word)
    return output_list


# wersja krótka #
def words_with_letter_a_short(list_of_words):
    return [word for word in list_of_words if 'a' in word.lower()]


if __name__ == '__main__':
    list_words = ["Admin", "labeda", "mysz", "jablko", "lot", "Apperance"]

    print(words_with_letter_a_long(list_words))
    print(words_with_letter_a_short(list_words))
