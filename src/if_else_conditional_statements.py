# Funkcja JEŻELI #
# jednowarunkowa #
def print_temperature_description_v1(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    else:  # jeśli mniej niż lub równe 25 #
        print("It's quite OK")


# wielowarunkowa #
def print_temperature_description_v2(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:  # powyżej 25 #
        print("It's getting hot!")
    elif temperature_in_celsius > 0:  # od 1 do 25 #
        print("It's quite OK")
    else:  # poniżej 1 #
        print("It's getting cold")


def is_person_an_adult_v1(age):
    if age >= 18:
        return True
    else:
        return False


# skrócona wersja #
def is_person_an_adult_v2(age):
    return True if age >= 18 else False


# == - lewa strona równa prawej #
# != - różne #


def normalne_warunki(temperatura, cisnienie):
    if temperatura == 0 and cisnienie == 1013:
        return True
    else:
        return False


def get_grade_for_test_percentage(test_percentage):
    if test_percentage >= 90:
        return 5
    elif test_percentage >= 75:
        return 4
    elif test_percentage >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    temperature_in_celsius = 0
    print_temperature_description_v1(temperature_in_celsius)
    print("-----")
    print_temperature_description_v2(temperature_in_celsius)
    print("-----")

    age_kate = 17.5
    age_tom = 18
    age_marta = 21

    print("Kate", is_person_an_adult_v1(age_kate))
    print("Tom", is_person_an_adult_v1(age_tom))
    print("Marta", is_person_an_adult_v1(age_marta))
    print("-----")
    print("Kate", is_person_an_adult_v2(age_kate))
    print("Tom", is_person_an_adult_v2(age_tom))
    print("Marta", is_person_an_adult_v2(age_marta))
    print("-----")
    print(normalne_warunki(1,1013))
    print("-----")
    punkty_ucznia = 25
    print(f"Wynik ucznia to {punkty_ucznia} pkt. Ocena ucznia to {get_grade_for_test_percentage(punkty_ucznia)}.")
