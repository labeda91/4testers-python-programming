# Faker - biblioteka generująca dane testowe #
from faker import Faker
# Random - generuje losowe liczby #
import random

fake = Faker()


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(),  # generuje losowe przykładowe maile #
        "seniority_years": random.randint(1, 40),  # generuje losowe liczby od 1 do 40 #
        "female": random.choice([True, False])  # generuje loso True lub False #
    }


# funkcję, która zwróci listę słowników z poprzedniego zadania o żądanej długości.
# Argumentem funkcji ma być ilość słowników w zwracanej liście.
def generate_array_of_employee_dictionaries(number_of_dictionaries):
    return [generate_random_employee_dictionary() for i in range(number_of_dictionaries)]


# Napisz funkcję, która przyjmie listę słowników wygenerowaną
# poprzednio i zwróci wszystkie emaile pracowników, którzy mają
# staż pracy (klucz seniority_years) większy niż 10.
def get_emails_of_employee_with_seniority_years_greather_than_10(list_of_employees):
    output_emails = []

    for employee_dictionary in list_of_employees:
        if employee_dictionary["seniority_years"] > 10:
            output_emails.append(employee_dictionary["email"])

    return output_emails

if __name__ == '__main__':
    for n in range(2):
        print(generate_random_employee_dictionary())

generated_employees = generate_array_of_employee_dictionaries(20)
print(generated_employees)

filtered_emails = get_emails_of_employee_with_seniority_years_greather_than_10(generated_employees)
print(filtered_emails)
