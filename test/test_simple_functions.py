from src.simple_functions import is_adult, words_with_letter_a_short


def test_is_adult_for_age_greatere_than_18():
    assert is_adult(19)  # == True - domyślnie jest True, więc nie trzeba tego pisać #


def test_is_adult_for_age_equal_18():
    assert is_adult(18)


def test_is_adult_for_age_less_than_18():
    assert not is_adult(17)  # zamiast == False można dać assert not #


# TESTY NA OBECNOŚĆ LITERY a LUB A #

# 1 PRZYPADEK TESTOWY - wszystkie słowa zawierają a lub A #
def test_list_containing_all_words_with_letter_a():
    input_lists = ["Admin", "labeda", "mysza", "jablko", "lotnia", "Apperance"]
    expected_lists = ["Admin", "labeda", "mysza", "jablko", "lotnia", "Apperance"]
    assert words_with_letter_a_short(input_lists) == expected_lists


# 2 PRZYPADEK TESTOWY - mix słów #
def test_list_containing_mixed_words():
    input_lists = ["Admin", "labeda", "mysza", "jablko", "lot", "Apperance", "why", "Miro"]
    expected_lists = ["Admin", "labeda", "mysza", "jablko", "Apperance"]
    assert words_with_letter_a_short(input_lists) == expected_lists


# 3 PRZYPADEK TESTOWY - puste listy #
def test_empty_list():
    input_lists = []
    expected_lists = []
    assert words_with_letter_a_short(input_lists) == expected_lists


# 4 PRZYPADEK TESTOWY - brak słów z literą a lub A #
def test_list_not_containing_words_with_letter_a():
    input_lists = ["lot", "why", "Miro"]
    expected_lists = []
    assert words_with_letter_a_short(input_lists) == expected_lists
