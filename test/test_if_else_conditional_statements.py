from src.if_else_conditional_statements import is_person_an_adult_v1


def test_age_over_18_years_old_should_be_an_adult():
    is_adult = is_person_an_adult_v1(19)
    assert is_adult


def test_age_18_years_old_should_be_an_adult():
    is_adult = is_person_an_adult_v1(18)
    assert is_adult


def test_age_under_18_years_old_should_be_an_adult():
    is_adult = is_person_an_adult_v1(17)
    assert not is_adult
