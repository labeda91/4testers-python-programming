from src.imports_employee_management import generate_random_employee_dictionary


def test_single_employee_generation_seniority_years_have_correct_value():
    test_employee = generate_random_employee_dictionary()
    employee_seniority_years = test_employee["seniority_years"]
    assert employee_seniority_years >= 1
    assert employee_seniority_years <= 40


def test_single_employee_generation_has_proper_keys():
    test_employee = generate_random_employee_dictionary()
    assert "email" in test_employee.keys()
    assert "seniority_years" in test_employee.keys()
    assert "female" in test_employee.keys()